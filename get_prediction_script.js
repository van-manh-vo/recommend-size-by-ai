// Normalize data
var heights = [];
var sizes = [];
var sizeEnums = ['XS', 'S', 'M', 'L', 'XL'];

mocked_data.map(item => {
    heights.push((item.height - 1400) / 550);
    sizes.push(sizeEnums.indexOf(item.size));
});

console.log(heights);

let x = tf.tensor1d(heights);
let sizesTensor = tf.tensor1d(sizes, 'int32');
let y = tf.oneHot(sizesTensor, sizeEnums.length);

// Create model
let model = tf.sequential();

let hiddenLayer = tf.layers.dense({
    units: 16,
    activation: "sigmoid",
    inputDim: 1 
});

let outputLayer = tf.layers.dense({
    units: 5,
    activation: "softmax",
});

model.add(hiddenLayer);
model.add(outputLayer);

model.compile({
    optimizer: tf.train.sgd(0.2),
    loss: "categoricalCrossentropy"
})

// Train model
async function train(){
    let options = {
        epochs: 2,
        validationSplit: 0.1,
        shuffle: true
    }
    return await model.fit(x, y, options);
}

console.log("Start Training...");

train().then((recommendation) => {
    console.log("Training Finished!");
});

// Get recommendation result
let heightTextBox = document.getElementById('height');
let getSizeRecommendationButton = document.getElementById('getSizeRecommendation');
let recommendationText = document.getElementById('recommendation');

getSizeRecommendationButton.addEventListener('click', function(){
    let height = heightTextBox.value;
    let heightInput = (height - 1400) / 550;
    let heightInputTensor = tf.tensor1d([heightInput])
    
    // Get prediction
    let prediction = model.predict(heightInputTensor);
    let matchedResult = prediction.argMax(1).dataSync()[0];

    recommendationText.innerHTML = "La taille : " + height + " convient avec : " + sizeEnums[matchedResult];
});